package kamil.potatoapocalipse;

/**
 * Created by Kamil on 24.11.2017.
 */

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Point;

public class PlayerClass {


    private Bitmap bitmap;
    private int width;
    private int height;

    private int x, y;

    private Point resolution;

    double angleprev;
    double angle;

    double xprev;
    double yprev;
    double x1;
    double y1;
    int hp;


    Matrix matrix;

    public PlayerClass(Context context) {
        resolution = new Point();
        width = 300;
        height = 300;
        hp = 300;


        bitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.potato);
        bitmap = Bitmap.createScaledBitmap(bitmap, width, height, true);


    }


    public void update(int x, int y) {

    }

    public Bitmap getBitmap() {

        //rotateBitmap(x, y);                    //Do zrobienia
        return bitmap;
    }

    public int getX() {

        return (x - bitmap.getWidth() / 2);
    }

    public int getY() {

        return (y - bitmap.getHeight() / 2);
    }

    public int getHp() {
        return hp;
    }

    public void decreaseHP(int decrease) {
        hp -= decrease;
    }

    private void rotateBitmap(int xtap, int ytap) {
        if (matrix == null) {
            matrix = new Matrix();
        }

        xprev = getX();
        yprev = getY();
        x1 = xtap;
        y1 = ytap;


        /*if (x1 - xprev > 0) {
            angle = ((Math.atan((y1 - yprev) / (x1 - xprev))) * 180 / 3.14) + 90;
        }
        if (x1 - xprev < 0) {
            angle = ((Math.atan((y1 - yprev) / (x1 - xprev))) * 180 / 3.14) - 90;
        }
        if (x1 - xprev == 0) {
            angle = angleprev;
        }*/
        angle = 45;

        //matrix.postRotate((float) angle);

        matrix.setRotate((float) angle, (float) width / 2, (float) height / 2);
        bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);

    }

    public void setResolution(Point resolution1) {
        resolution.x = resolution1.x;
        resolution.y = resolution1.y;
        x = resolution.x / 2;
        y = resolution.y / 2;
    }

}

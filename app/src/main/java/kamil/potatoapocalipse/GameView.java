package kamil.potatoapocalipse;

/**
 * Created by Kamil on 24.11.2017.
 */

import android.content.Context;
import android.content.ReceiverCallNotAllowedException;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.PointF;
import android.graphics.Rect;
import android.util.DisplayMetrics;
import android.util.SparseArray;
import android.view.Display;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.WindowManager;

import java.util.ArrayList;
import java.util.Random;

public class GameView extends SurfaceView implements Runnable {

    boolean playing;
    private Thread gameThread = null;
    private PlayerClass player;
    private EnemyClass enemy;

    private Paint paint;
    private Canvas canvas;
    private SurfaceHolder surfaceHolder;
    private WindowManager wm;
    private Display display;
    private int height;
    private int width;
    private Point resolution;
    private Point tapPoint;

    //private EnemyClass[] enemies;
    private ArrayList<EnemyClass> enemies;
    private int enemiesCount;

    private ArrayList<BulletClass> bullets;

    private int timets;  //time to shoot
    private int weapon;

    private int score;

    Bitmap background;
    Rect dest;



    public GameView(Context context) {
        super(context);

        playing = true;
        surfaceHolder = getHolder();
        paint = new Paint();
        paint.setAntiAlias(true);
        paint.setFilterBitmap(true);
        paint.setDither(true);

        tapPoint = new Point();


        //Pobranie rozdzielczości ekranu
        resolution = new Point();
        wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        display = wm.getDefaultDisplay();
        display.getSize(resolution);
        width = resolution.x;
        height = resolution.y;
        //koniec pobrania

        player = new PlayerClass(context);
        player.setResolution(resolution);


        Random generator = new Random();

        // Generowanie liczby przeciwników z przedziału
        enemiesCount = (generator.nextInt(10)) + 1;
        enemies = new ArrayList<EnemyClass>();
        for (int i = 0; i < enemiesCount; i++) {
            EnemyClass enemy = new EnemyClass(context);
            enemies.add(enemy);
            enemies.get(i).setResolution(resolution);
        }
        // koniec pobrania

        bullets = new ArrayList<BulletClass>();
        timets = 0;
        weapon = 1;

        background = BitmapFactory.decodeResource(getResources(), R.drawable.bullet);

        dest = new Rect(0, 0, getWidth(), getHeight());


    }


    @Override
    public void run() {
        while (playing == true) {   //Główna pętla programu
            update();
            draw();
            control();
        }
    }

    private void update() {
        player.update(tapPoint.x, tapPoint.y);

        for (int i = 0; i < enemies.size(); i++) {
            enemies.get(i).update(player);
            if (!enemies.get(i).getAlive()) {
                score += enemies.get(i).getScore();
                enemies.remove(i);
            }
        }
        for (int i = 0; i < bullets.size(); i++) {
            bullets.get(i).update(enemies);
            if (!bullets.get(i).checkAlive()) {
                bullets.remove(i);
            }
        }
        timets++;
        if (player.getHp() == 0) {
            playing = false;
        }
    }

    private void draw() {
        if (surfaceHolder.getSurface().isValid()) {
            canvas = surfaceHolder.lockCanvas();
            canvas.drawColor(Color.rgb(84, 183, 89));
            //canvas.drawBitmap(background, null, dest, paint);


            paint.setTextSize(30);
            canvas.drawText("HP: " + player.getHp() + "      " + "Score: " + score, canvas.getWidth() / 2, 50, paint);


            canvas.drawBitmap(player.getBitmap(), player.getX(), player.getY(), paint);
            for (int i = 0; i < enemies.size(); i++) {
                if (enemies.get(i).getAlive()) {
                    canvas.drawBitmap(enemies.get(i).getBitmap(), enemies.get(i).getX(), enemies.get(i).getY(), paint);
                }
            }

            for (int i = 0; i < bullets.size(); i++) {
                canvas.drawBitmap(bullets.get(i).getBitmap(), bullets.get(i).getX(), bullets.get(i).getY(), paint);
            }


            if (player.getHp() == 0) {
                paint.setTextSize(150);
                paint.setTextAlign(Paint.Align.CENTER);

                int yPos = (int) ((canvas.getHeight() / 2) - ((paint.descent() + paint.ascent()) / 2));
                canvas.drawText("Game Over", canvas.getWidth() / 2, yPos, paint);

            }

            if (enemies.size() == 0) {
                paint.setTextSize(150);
                paint.setTextAlign(Paint.Align.CENTER);

                int yPos = (int) ((canvas.getHeight() / 2) - ((paint.descent() + paint.ascent()) / 2));
                canvas.drawText("You Survived", canvas.getWidth() / 2, yPos, paint);

            }



            surfaceHolder.unlockCanvasAndPost(canvas);
        }

    }

    private void control() {
        try {
            gameThread.sleep(17);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    public void resume() {
        playing = true;
        gameThread = new Thread(this);
        gameThread.start();
    }

    public void pause() {
        playing = false;
        try {
            gameThread.join();
        } catch (InterruptedException e) {
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent motionEvent) {

        switch (motionEvent.getAction() & motionEvent.ACTION_MASK) {
            case MotionEvent.ACTION_DOWN:
                tapPoint.x = (int) motionEvent.getX();
                tapPoint.y = (int) motionEvent.getY();
                break;


            case MotionEvent.ACTION_UP:

                break;

            case MotionEvent.ACTION_MOVE:
                tapPoint.x = (int) motionEvent.getX();
                tapPoint.y = (int) motionEvent.getY();
                break;
        }
        switch (weapon) {
            case 1:
                BulletClass bullet = new BulletClass(getContext(), 1, tapPoint.x, tapPoint.y);
                if (bullet.getRldtime() <= timets) {
                    bullet.setResolution(resolution);
                    bullets.add(bullet);
                    timets = 0;
                } else {
                    bullet = null;
                }
                break;

        }


        return true;
    }


}

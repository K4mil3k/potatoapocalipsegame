package kamil.potatoapocalipse;

/**
 * Created by Kamil on 24.11.2017.
 */

import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;

public class GameActivity extends Activity {

    private GameView gameView;
    MediaPlayer ring;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        gameView = new GameView(this);
        setContentView(gameView);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);

        ring = MediaPlayer.create(GameActivity.this, R.raw.maintheme); // Soundtrack
        ring.setLooping(true);
        //ring.start();

    }

    protected void onStart() {
        super.onStart();
        // ring.start();
    }

    @Override
    protected void onPause() {
        super.onPause();
        gameView.pause();
        ring.pause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        gameView.resume();
        ring.start();
    }

    @Override
    protected void onStop() {
        super.onStop();
        ring.pause();
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus){
            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
        }

    }
}

package kamil.potatoapocalipse;

/**
 * Created by Kamil on 24.12.2017.
 */

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.graphics.Rect;

import java.util.Random;

public class EnemyClass {

    private Bitmap bitmap;
    private int width;
    private int height;

    private float x, y;

    private Point resolution;

    double angleprev;
    double angle;

    protected float xact, yact;

    private int hp, dmg, speed, hbr, score;  //HitBox Radius

    private float distance;
    private boolean alive;
    private int enemyType;


    public EnemyClass(Context context) {   //generowanie losowego przeciwnika i wspolrzednych w konstuktorze
        resolution = new Point();
        alive = true;

        Random generator = new Random();
        enemyType = generator.nextInt(3);


        switch (enemyType) {
            case 0:
                width = 300;
                height = 300;

                speed = 50;
                hp = 100;
                hbr = 100;
                dmg = 1;
                score = 100;

                bitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.zombie1);
                bitmap = Bitmap.createScaledBitmap(bitmap, width, height, true);
                break;

            case 1:
                width = 300;
                height = 300;

                speed = 80;
                hp = 30;
                hbr = 100;
                dmg = 1;
                score = 20;

                bitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.zombie2);
                bitmap = Bitmap.createScaledBitmap(bitmap, width, height, true);

                break;

            case 2:
                width = 200;
                height = 200;

                speed = 60;
                hp = 70;
                hbr = 100;
                dmg = 1;
                score = 70;

                bitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.zombie3);
                bitmap = Bitmap.createScaledBitmap(bitmap, width, height, true);

                break;



        }

    }

    public void update(PlayerClass player) {
        goTo();
        checkAlive();
        hurtPlayer(player);


    }

    public void setResolution(Point resolution1) {
        resolution.x = resolution1.x;
        resolution.y = resolution1.y;
        x = resolution.x / 2;
        y = resolution.y / 2;
        Random generator = new Random();
        xact = generator.nextInt(resolution.x);
        yact = generator.nextInt(resolution.y);
    }

    public Bitmap getBitmap() {

        //rotateBitmap(x, y);                    //Do zrobienia
        return bitmap;
    }

    public float getX() {

        return (xact - bitmap.getWidth() / 2);
    }

    public float getY() {

        return (yact - bitmap.getHeight() / 2);
    }

    public float getXact() {
        return xact;
    }

    public float getYact() {
        return yact;
    }

    public int getScore() {
        return score;
    }

    private void goTo() {


        countDistance(xact, yact, x, y);

        float xStep = (x - xact) / distance * speed / 100;
        float yStep = (y - yact) / distance * speed / 100;

        if (distance == 0) {
            xStep = 0;
            yStep = xStep;
        }

        xact += xStep;
        yact += yStep;


    }

    private float countDistance(float x1, float y1, float x2, float y2) {

        distance = (float) Math.sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2));

        return distance;
    }

    public int getHbr() {
        return hbr;
    }

    public void decreaseHp(int decrease) {
        hp -= decrease;

    }

    private void checkAlive() {
        if (hp <= 0) {
            alive = false;
        }
    }

    public boolean getAlive() {
        return alive;
    }

    private void hurtPlayer(PlayerClass player) {

        if (countDistance(getXact(), getYact(), x, y) <= hbr) {
            player.decreaseHP(dmg);
        }
    }

}

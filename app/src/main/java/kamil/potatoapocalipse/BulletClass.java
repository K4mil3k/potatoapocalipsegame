package kamil.potatoapocalipse;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by Kamil on 26.12.2017.
 */

public class BulletClass {

    private Bitmap bitmap;
    private int width;
    private int height;

    private float xact, yact;

    private float xend, yend;

    private Point resolution;

    private int damage;

    private boolean isAlive;

    private float distance;

    private int speed, dmg, rldtime;  // speed, damage, reload time
    private boolean fire;


    public BulletClass(Context context, int type, int xend, int yend) {
        resolution = new Point();
        width = 100;
        height = 100;
        isAlive = true;

        this.xend = xend;
        this.yend = yend;


        switch (type) {

            case 1:
                speed = 5000;
                dmg = 10;
                rldtime = 10;
                bitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.bullet);
                bitmap = Bitmap.createScaledBitmap(bitmap, width, height, true);
                break;
            case 2:


                break;
        }
    }

    public void update(ArrayList<EnemyClass> enemies) {
        goTo();
        checkBorder();
        checkCollision(enemies);


    }

    public void setResolution(Point resolution1) {
        resolution.x = resolution1.x;
        resolution.y = resolution1.y;
        xact = resolution.x / 2;
        yact = resolution.y / 2;

    }

    public Bitmap getBitmap() {

        //rotateBitmap(x, y);                    //Do zrobienia
        return bitmap;
    }

    public float getX() {

        return (xact - bitmap.getWidth() / 2);
    }

    public float getY() {

        return (yact - bitmap.getHeight() / 2);
    }

    public float getXact(){
        return xact;
    }

    public float getYact(){
        return yact;
    }

    public int getRldtime(){
        return rldtime;
    }



    private void goTo() {


        countDistance(resolution.x / 2, resolution.y / 2, xend, yend);

        float xStep = (xend - resolution.x / 2) / distance * speed / 100;
        float yStep = (yend - resolution.y / 2) / distance * speed / 100;

        if (distance == 0) {
            xStep = 0;
            yStep = xStep;

        }

        xact += xStep;
        yact += yStep;


    }

    private float countDistance(float x1, float y1, float x2, float y2) {

        distance = (float) Math.sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2));

        return distance;
    }

    public void setAlive0() {  // NOT USED
        isAlive = false;
    }

    public boolean checkAlive() {
        return isAlive;
    }

    private void checkBorder() {
        if (xact > resolution.x - width / 2 || xact < 0 - width / 2 || yact > resolution.y - height / 2 || yact < 0 - height / 2) {
            isAlive = false;
        }
    }

    private boolean checkRldTime() {

        return fire;
    }

    private void checkCollision(ArrayList<EnemyClass> enemies) {
        float min = 9999;
        int imin = 0;
        for (int i = 0; i < enemies.size(); i++) {
            if(enemies.get(i).getAlive()){
            if (countDistance(getXact(), getYact(), enemies.get(i).getXact(), enemies.get(i).getYact()) <= min) {
                min = countDistance(getX(), getY(), enemies.get(i).getXact(), enemies.get(i).getYact());
                imin = i;
            }
        }
        if (min <= enemies.get(imin).getHbr()) {
            enemies.get(imin).decreaseHp(dmg);
            setAlive0();}

        }


    }

}
